package com.parking;

public class CannotFindCarException extends Throwable {
    public CannotFindCarException(String message) {
        super(message);
    }
}
