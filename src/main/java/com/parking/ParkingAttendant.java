package com.parking;

import java.util.ArrayList;
import java.util.List;

public class ParkingAttendant {

    private List<ParkingLot> parkingLotList = new ArrayList<>();

    ParkingAttendant(List parkingLotList) {
        this.parkingLotList = parkingLotList;
    }


    public String park(Object car) throws CannotParkException {
        String slotId = null;
        if (parkingLotList.size() == 0)
            throw new CannotParkException("No ParkingLots Exists");
        for (int index = 0; index < parkingLotList.size(); index++) {
            if (!parkingLotList.get(index).isCapacityFull()) {
                slotId = parkingLotList.get(index).park(car);
                break;
            }
        }
        if (slotId == null) throw new CannotParkException("All ParkingLots are full");
        return slotId;

    }

    public Object unPark(String slotId) throws CannotUnparkException, CannotParkException {
        Object car = null;
        for (int index = 0; index < parkingLotList.size(); index++) {
            if (parkingLotList.get(index).checkIfSlotIdExists(slotId)) {
                car = parkingLotList.get(index).unPark(slotId);
                break;
            }
        }
        if (car == null) throw new CannotUnparkException("Car Not Found");
        return car;
    }

    public String findCar(Object car) throws CannotFindCarException {
        String parkingLotInstance = null;

        for (int index = 0; index < parkingLotList.size(); index++) {
            ParkingLot parkingLotObject=parkingLotList.get(index);
            if (parkingLotObject.findMyCar(car)!=null) {
                parkingLotInstance=parkingLotObject.toString()+" ";
                parkingLotInstance=parkingLotInstance+parkingLotObject.findMyCar(car);
            }
        }
        if (parkingLotInstance == null) throw new CannotFindCarException("Car Not Found");
        return parkingLotInstance;

    }

}
