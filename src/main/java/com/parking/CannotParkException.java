package com.parking;

    public class CannotParkException extends Exception {
        public CannotParkException(String message) {
            super(message);
        }
    }

