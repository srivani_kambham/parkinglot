package com.parking;

public class CannotUnparkException extends Exception{
    public CannotUnparkException(String message) {
        super(message);
    }
}
