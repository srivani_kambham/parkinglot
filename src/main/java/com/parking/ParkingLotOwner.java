package com.parking;

public class ParkingLotOwner  {
    private final static String PARKINGFULL = "Parking Full";
     private String displayCard;

     void updateParkingLotOwner(boolean status)
     {
         displayCard = status?PARKINGFULL:"";
     }

    @Override
    public String toString() {
        return displayCard;
    }
}
