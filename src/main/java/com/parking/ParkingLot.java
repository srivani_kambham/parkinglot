package com.parking;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class ParkingLot{
    private int capacity;
    private ParkingLotOwner owner;
    private HashMap<String, Object> parkedCars = new HashMap<>();

    ParkingLot(int capacity,ParkingLotOwner owner) {
        this.capacity = capacity;
        this.owner=owner;
    }

    public String park(Object car) throws CannotParkException {
        if (isCapacityFull())
            throw new CannotParkException("All slots are full");
        if (parkedCars.containsValue(car))
            throw new CannotParkException("Car is already parked");
        String slotId = UUID.randomUUID().toString();
        parkedCars.put(slotId, car);
        if (isCapacityFull())
            owner.updateParkingLotOwner(isCapacityFull());
        return slotId;
    }


    public Object unPark(String slotId) throws CannotParkException, CannotUnparkException {
        boolean isFull = isCapacityFull();
        if (!checkIfSlotIdExists(slotId))
            throw new CannotUnparkException("Car doesn't exist");
        Object car = parkedCars.remove(slotId);
        if(car==null)
            throw new CannotParkException("Unpark failed");
           if(isFull) owner.updateParkingLotOwner(isCapacityFull());
        return car;
    }

    boolean isCapacityFull() {
        return parkedCars.size() == capacity;
    }

    public boolean checkIfSlotIdExists(String slotId){
        return parkedCars.containsKey(slotId);
    }


    public String findMyCar(Object car) throws CannotFindCarException {
        String slotId=null;
        for(Map.Entry<String, Object> entry: parkedCars.entrySet()) {
            if(entry.getValue() == car) {
                slotId=entry.getKey();
                break;
            }
        }
        if(slotId==null) throw new CannotFindCarException("Car Not Found");
        return slotId;

    }
}


