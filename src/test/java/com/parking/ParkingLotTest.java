package com.parking;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;


public class ParkingLotTest {
    ParkingLot parkingLot;
    ParkingLotOwner parkingLotOwner;

    @BeforeEach
    void setUp() {
        parkingLotOwner = mock(ParkingLotOwner.class);
        parkingLot = new ParkingLot(3, parkingLotOwner);
    }

    @Test
    void shouldParkACar() throws CannotParkException {
        Object car = new Object();
        assertNotNull(parkingLot.park(car));
    }

    @Test
    void shouldParkTwoCars() throws CannotParkException {
        Object car1 = new Object();
        Object car2 = new Object();
        assertNotNull(parkingLot.park(car1));
        assertNotNull(parkingLot.park(car2));
    }

    @Test
    void shouldNotParkACarIfNoSlotsAvailable() throws CannotParkException {
        Object car1 = new Object();
        Object car2 = new Object();
        Object car3 = new Object();
        Object car4 = new Object();
        parkingLot.park(car1);
        parkingLot.park(car2);
        parkingLot.park(car3);
        assertThrows(CannotParkException.class, () -> parkingLot.park(car4), "Existing Slots are full so cannot park");
    }

    @Test
    void shouldNotParkSameCarAgain() throws CannotParkException {
        Object car1 = new Object();
        parkingLot.park(car1);
        assertThrows(CannotParkException.class, () -> parkingLot.park(car1), "Can not park same car again");
    }

    @Test
    void shouldUnParkACar() throws CannotParkException, CannotUnparkException {
        Object car1 = new Object();
        String slotId = parkingLot.park(car1);
        assertEquals(car1, parkingLot.unPark(slotId));
    }

    @Test
    void shouldNotUnparkAnUnParkedCar() throws CannotParkException, CannotUnparkException {
        Object car1 = new Object();
        String slotId = parkingLot.park(car1);
        parkingLot.unPark(slotId);
        assertThrows(CannotUnparkException.class, () -> parkingLot.unPark(slotId), "Car doesn't exist");
    }

    @Test
    void shouldCheckIfParkingLotIsFull() throws CannotParkException {
        Object car1 = new Object();
        Object car2 = new Object();
        Object car3 = new Object();
        parkingLot.park(car1);
        parkingLot.park(car2);
        parkingLot.park(car3);
        Assertions.assertTrue(parkingLot.isCapacityFull());

    }

    @Test
    void shouldNotifyOwnerIfParkingLotIsFull() throws CannotParkException {
        Object car1 = new Object();
        Object car2 = new Object();
        Object car3 = new Object();
        parkingLot.park(car1);
        parkingLot.park(car2);
        parkingLot.park(car3);
        verify(parkingLotOwner, times(1)).updateParkingLotOwner(true);

    }

    @Test
    void shouldNotifyOwnerIfParkingLotIsAvailable() throws CannotParkException, CannotUnparkException {
        Object car1 = new Object();
        Object car2 = new Object();
        Object car3 = new Object();
        parkingLot.park(car1);
        parkingLot.park(car2);
        String lotId = parkingLot.park(car3);
        verify(parkingLotOwner, times(1)).updateParkingLotOwner(true);
        parkingLot.unPark(lotId);
        verify(parkingLotOwner, times(1)).updateParkingLotOwner(false);
    }


    @Test
    void shouldCheckIfSlotIdExists() throws CannotParkException {
        Object car = new Object();
        String slotId = parkingLot.park(car);
        Assertions.assertTrue(parkingLot.checkIfSlotIdExists(slotId));
    }

    @Test
    void shouldBeAbleToFindMyCar() throws CannotParkException, CannotFindCarException{
        Object car = new Object();
        String slotId = parkingLot.park(car);
        assertEquals(slotId, parkingLot.findMyCar(car));
    }

    @Test
    void shouldNotBeAbleToFindMyCar() {
        Object car = new Object();
        assertThrows(CannotFindCarException.class, () -> parkingLot.findMyCar(car));
    }

}

