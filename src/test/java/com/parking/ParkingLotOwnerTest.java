package com.parking;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ParkingLotOwnerTest {
    @Test
    public void shouldDisplayFullSignMessage(){
        ParkingLotOwner owner = new ParkingLotOwner();
        owner.updateParkingLotOwner(true);
        assertEquals("Parking Full",owner.toString());
    }
    @Test
    public void shouldRemoveFullSignMessage(){
        ParkingLotOwner owner = new ParkingLotOwner();
        owner.updateParkingLotOwner(true);
        owner.updateParkingLotOwner(false);
        assertTrue(owner.toString().isEmpty());
    }


}
