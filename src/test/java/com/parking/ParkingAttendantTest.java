package com.parking;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;

public class ParkingAttendantTest {

    ParkingAttendant parkingAttendant;
    List<ParkingLot> parkingLotList = new ArrayList<>();
    ParkingLotOwner parkingLotOwner;

    @BeforeEach
    void setUp() {

        parkingLotOwner = mock(ParkingLotOwner.class);
        parkingAttendant = new ParkingAttendant(parkingLotList);
    }


    @Test
    void shouldBeAbleToPark() throws CannotParkException {

        parkingLotList.add(new ParkingLot(3, parkingLotOwner));
        Object car = new Object();
        assertNotNull(new ParkingAttendant(parkingLotList).park(car));
    }

    @Test
    void shouldBeAbleToParkInMultipleParkingLots() throws CannotParkException {

        parkingLotList.add(new ParkingLot(2, parkingLotOwner));
        parkingLotList.add(new ParkingLot(1, parkingLotOwner));
        Object car1 = new Object();
        Object car2 = new Object();
        Object car3 = new Object();
        assertNotNull(new ParkingAttendant(parkingLotList).park(car1));
        assertNotNull(new ParkingAttendant(parkingLotList).park(car2));
        assertNotNull(new ParkingAttendant(parkingLotList).park(car3));
    }

    @Test
    void shouldThrowExceptionIfAllParkingLotsAreFull() throws CannotParkException {

        parkingLotList.add(new ParkingLot(1, parkingLotOwner));
        parkingLotList.add(new ParkingLot(1, parkingLotOwner));
        Object car1 = new Object();
        Object car2 = new Object();
        Object car3 = new Object();
        assertNotNull(new ParkingAttendant(parkingLotList).park(car1));
        assertNotNull(new ParkingAttendant(parkingLotList).park(car2));
        assertThrows(CannotParkException.class, () -> new ParkingAttendant(parkingLotList).park(car3));
    }

    @Test
    void shouldThrowExceptionIfNoParkingLotsExist() throws CannotParkException {
        Object car = new Object();
        assertThrows(CannotParkException.class, () -> new ParkingAttendant(parkingLotList).park(car));
    }

    @Test
    void shouldUnParkACar() throws CannotParkException, CannotUnparkException {
        Object car1 = new Object();
        parkingLotList.add(new ParkingLot(3, parkingLotOwner));
        String slotId = new ParkingAttendant(parkingLotList).park(car1);
        assertEquals(car1, parkingAttendant.unPark(slotId));
    }

    @Test
    void shouldUnParkACarFromMultipleParkingLots() throws CannotParkException, CannotUnparkException {
        Object car1 = new Object();
        Object car2 = new Object();
        Object car3 = new Object();
        parkingLotList.add(new ParkingLot(3, parkingLotOwner));
        parkingLotList.add(new ParkingLot(1, parkingLotOwner));
        parkingLotList.add(new ParkingLot(2, parkingLotOwner));
        String slotId1 = new ParkingAttendant(parkingLotList).park(car1);
        String slotId2 = new ParkingAttendant(parkingLotList).park(car2);
        String slotId3 = new ParkingAttendant(parkingLotList).park(car3);
        assertEquals(car3, parkingAttendant.unPark(slotId3));
        assertEquals(car2, parkingAttendant.unPark(slotId2));
        assertEquals(car1, parkingAttendant.unPark(slotId1));
    }

    @Test
    void shouldThrowExceptionWhileUnParkingACarFromMultipleParkingLotsWithInvalidSlotId() throws CannotParkException, CannotUnparkException {
        Object car1 = new Object();
        Object car2 = new Object();
        Object car3 = new Object();
        parkingLotList.add(new ParkingLot(1, parkingLotOwner));
        parkingLotList.add(new ParkingLot(1, parkingLotOwner));
        parkingLotList.add(new ParkingLot(2, parkingLotOwner));
        String slotId1 = new ParkingAttendant(parkingLotList).park(car1);
        String slotId2 = new ParkingAttendant(parkingLotList).park(car2);
        String slotId3 = new ParkingAttendant(parkingLotList).park(car3);
        assertEquals(car3, parkingAttendant.unPark(slotId3));
        assertThrows(CannotUnparkException.class, () -> new ParkingAttendant(parkingLotList).unPark(slotId3));
    }


    @Test
    void shouldBeAbleToFindMyCar() throws CannotParkException, CannotFindCarException {
        Object car=new Object();
        parkingLotList.add(new ParkingLot(1, parkingLotOwner));
        String slotId=new ParkingAttendant(parkingLotList).park(car);
        assertEquals(parkingLotList.get(0).toString()+" "+slotId,parkingAttendant.findCar(car));
    }

    @Test
    void shouldNotBeAbleToFindMyCar() {
        Object car=new Object();
        assertThrows(CannotFindCarException.class, () ->parkingAttendant.findCar(car));
    }


}
